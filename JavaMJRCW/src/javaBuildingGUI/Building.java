package javaBuildingGUI;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

/**
 * The class for the Building, containing the rooms and objects.
 * 
 * @author Will
 *
 */
public class Building {

	private int xSize = 0, ySize = 0; // size of building in x and y
	private ArrayList<Room> allRooms; // arraylist of rooms
	private ArrayList<Thing> allObj; // arraylist of objects
	private String[] Info, bSize; // Info string, and Building size string
	private ArrayList<Person> people; // arraylist of people
	private int pplAmt = 1; // starting amount of people
	public int objAmt = 0; // starting amount of objects

	/**
	 * Constructor for the building
	 * 
	 * @param S The building string that determines the building dimensions
	 */
	Building(String S) {
		setBuilding(S);
		setPeople(pplAmt); // Sets the starting amount of people.
	}

	/**
	 * Show Function that draws all rooms, people and objects.
	 * 
	 * @param g The GUI that the building is drawn on.
	 */
	public void Show(GUI g) {
		for (int x = 0; x < allRooms.size(); x++) { // for all rooms, call show method
			allRooms.get(x).show(g, x + 1);
		}
		for (Person p : people) // for all people, call show method
			p.show(g);
		for (Thing o : allObj) // for all objects, call show method
			o.show(g);
	}

	/**
	 * A Function to add a person to the building.
	 */
	public void addPerson() {
		pplAmt++; // incrementing the people amount
		Random randGen = new Random();
		int room = randGen.nextInt(allRooms.size()); // Generating random number based on no.ofRooms
		Point P = allRooms.get(room).getRandom(randGen); // making a random point from that.
		Person Person = new Person(P);
		people.add(Person); // Adding the person
	}

	/**
	 * A function to add an object to the building
	 * 
	 * @param objChoice A string of the object passed to it.
	 */
	public void addObject(String objChoice) {
		Random randGen = new Random(); // Making a random object
		int room = randGen.nextInt(allRooms.size()); // Finding a random room
		Point P = allRooms.get(room).getRandom(randGen); // Finding a random point in that room
		if (objChoice == "Bed") { // if adding a bed
			P = allRooms.get(room).getTLCorner(); // make the point the Top Left corner
			while (checkObjPos(P) == false) { // while the bed can't be put down
				if (checkObjAmt("Bed") == false) {
					break; // Stops any additional beds being added
				}
				room = randGen.nextInt(allRooms.size()); // find another random room
				P = allRooms.get(room).getTLCorner(); // find that point's TL corner
			}
			if (checkObjAmt("Bed") != false) { // if there is space for another bed
				objAmt++;
				Bed newThing = new Bed(P);
				allObj.add(newThing); // add bed to the building
			}
		} else if (objChoice == "Table") { // If added object is table
			objAmt++;
			Table newThing = new Table(P);
			allObj.add(newThing); // Add it to the building
		} else if (objChoice == "Fridge") { // If added object is a fridge
			objAmt++;
			Fridge newFridge = new Fridge(P);
			allObj.add(newFridge); // Add to the building
		} else if (objChoice == "Light") { // If added object is a light
			P = allRooms.get(room).getMid(); // Make the point the mid point of the room
			while (checkObjPos(P) == false) { // Like the bed
				if (checkObjAmt("Light") == false) {
					break;
				}
				room = randGen.nextInt(allRooms.size()); // Keep finding a point it can go
				P = allRooms.get(room).getMid();
			}
			if (checkObjAmt("Light") != false) {
				objAmt++;
				Light newLight = new Light(P);
				allObj.add(newLight); // Add the light to the building
			}
		} else if (objChoice == "Sink") { // If added object is the sink
			objAmt++;
			Sink newThing = new Sink(P);
			allObj.add(newThing); // Add to the building
		}
	}

	/**
	 * Function to determine if an object will be placed on top of another.
	 * 
	 * @param P The point the new object could be placed.
	 * @return boolean to say if object can be placed or not.
	 */
	private boolean checkObjPos(Point P) {
		int ArraySZ = allObj.size();
		for (int x = 0; x < ArraySZ; x++) { // check all objects
			if (allObj.get(x).getPos().x > P.x - 10 && allObj.get(x).getPos().y > P.y - 10 // if the point is on an
																							// object +/- 10
					&& allObj.get(x).getPos().x < P.x + 10 && allObj.get(x).getPos().y < P.y + 10) {
				return false; // object can't go there.
			}
		}
		return true; // otherwise object can go there
	}

	/**
	 * Function to determine if an object that can only be in a room once can be
	 * added
	 * 
	 * @param objType Type of object to be added
	 * @return boolean to say if there's too many of that object or not.
	 */
	private boolean checkObjAmt(String objType) {
		int count = 0;
		for (int x = 0; x < allObj.size(); x++) { // for all objects
			if (allObj.get(x).getName() == objType)
				count++; // count how many of that object type there is
			if (count >= allRooms.size()) // if there's too many of that object
				return false; // object can't be added
		}
		return true; // otherwise, object can be added
	}

	/**
	 * Function to set people in the building at random points
	 * 
	 * @param pplAmt amount of people due to be set
	 */
	public void setPeople(int pplAmt) {
		people.clear(); // clear arraylist
		for (int ct = 0; ct < pplAmt; ct++) {// room number the person is in.
			Random randGen = new Random(); // Making a random object
			int room = randGen.nextInt(allRooms.size()); // Generating random number based on no.ofRooms
			Point P = allRooms.get(room).getRandom(randGen); // making a random point from that.
			Person Person = new Person(P);
			people.add(Person); // Add person
		}
	}

	/**
	 * Function to call the person to call their move method
	 * 
	 * @param person the person due to move
	 * @return a value to determine if the person has finished moving or not.
	 */
	public int mPerson(Person person) { // Move person function
		int x;
		x = person.movePerson(); // will return a value of 0 or -1
		if (x != 0) { // -1 means the path has been finished.
			return -1; // returns -1 to the "animate" function
		} else
			return 0; // returns a 0 to the animate function

	}

	/**
	 * Returns the list of people
	 * 
	 * @return people list
	 */
	public ArrayList<Person> getPrsnList() {
		return people;
	}

	/**
	 * Returns the list of all objects in the building.
	 * 
	 * @return object list
	 */
	public ArrayList<Thing> getObjList() {
		return allObj;
	}

	/**
	 * Returns the list of all rooms in the building.
	 * 
	 * @return room list
	 */
	public ArrayList<Room> getRoomList() {
		return allRooms;
	}

	/**
	 * Returns a person's path size
	 * 
	 * @param person the person whose path size is to be found
	 * @return calls the function for the person
	 */
	public int getPathSize(Person person) {
		return person.getPathSize();
	}

	/**
	 * Returns amount of people in the building currently
	 * 
	 * @return number of people in building
	 */
	public int getPAmt() {
		return pplAmt;
	}

	/**
	 * A function to add a path to a given person. Person's path is out of current
	 * room, to a random point of another room.
	 * 
	 * @param prsNo The person whose path is added
	 */
	public void addPath(int prsNo) {
		Person P = people.get(prsNo);
		int currm = roomLoc(P); // Current Room number
		int newrm = randRoom(); // Random Room Number
		while (currm == newrm) { // if the randomly selected room is the same as the current one, pick another
			newrm = randRoom();
		}
		P.createPathvar(); // Create the person
		P.clearPath(); // Clear the Path
		if (currm != -1) {
			P.addPathPt(allRooms.get(currm).returnDoor(-1)); // Add the point before the door of current room.
			P.addPathPt(allRooms.get(currm).returnDoor(1)); // Add the point after the door of current room.
		}
		P.addPathPt(allRooms.get(newrm).returnDoor(1)); // Add the point after the door of the new room.
		P.addPathPt(allRooms.get(newrm).returnDoor(-1)); // Add the point before the door of the new room.
		Random O = new Random();
		P.addPathPt(allRooms.get(newrm).getRandom(O));
	}

	/**
	 * Returns the Size of Building in the X coordinate
	 * 
	 * @return Size in x coordinate
	 */
	public int getX() { // Get size of building in x coordinate
		return xSize;
	}

	/**
	 * Returns the Size of Building in the Y coordinate
	 * 
	 * @return Size in y coordinate
	 */
	public int getY() { // Get size of building in y coordinate
		return ySize;
	}

	/**
	 * Function to set up the building.
	 * 
	 * @param bS the Building string passed to it.
	 */
	public void setBuilding(String bS) {
		allRooms = new ArrayList<Room>();
		allRooms.clear(); // clear the current array list
		Info = bS.split(";"); // split bS by ";"
		bSize = Info[0].split(" "); // use first string to set xSize and ySize
		xSize = Integer.parseInt(bSize[0]); // for all the remaining strings
		ySize = Integer.parseInt(bSize[1]);
		for (int x = 1; x < Info.length; x++) {
			allRooms.add(new Room(Info[x])); // Does a for loop to add the room to the arraylist.
		}
		people = new ArrayList<Person>();
		setPeople(pplAmt); // Sets the person
		allObj = new ArrayList<Thing>();
	}

	/**
	 * Function to return the room of an Object.
	 * 
	 * @param p Object that's room needs to be found
	 * @return the number of the room
	 */
	public int roomLoc(Thing p) {
		for (int x = 0; x < allRooms.size(); x++) { // steps through each room
			if ((allRooms.get(x).isInRoom(p.getPos())) == true)
				return x; // if "isInRoom" is true, return the room no.
		}
		return -1; // otherwise, return -1.
	}

	/**
	 * A function to build the building string
	 * 
	 * @return The building string
	 */
	public String toString() {
		String s = "";
		s = s + "Building Size " + xSize + "," + ySize + "\n";
		for (Room r : allRooms) // for all rooms
			s = s + r.toString() + "\n"; // add room info
		s = s + "\n";
		for (Person p : people) // for all people
			s = s + "Person " + (people.indexOf(p) + 1) + " is in room " + (roomLoc(p) + 1) + "\n"; // add person, and
																									// their room
																									// location
		for (Person p : people) // for all people (again)
			s = s + "Person " + (people.indexOf(p) + 1) + p.toString() + "\n"; // add person and info
		s = s + "\n";
		for (Thing p : allObj) // for all objects
			s = s + "ID: " + p.getID() + ", " + p.getName() + " is in room " + (roomLoc(p) + 1) + "\n"; // add obj and
																										// room location
		return s;
	}

	/**
	 * Function to call for a person to clear their path.
	 * 
	 * @param prsNo index of person to clear their path.
	 */
	public void rmvPath(int prsNo) {
		Person P = people.get(prsNo);
		P.clearPath();
	}

	/**
	 * Function to find a random room of the building.
	 * 
	 * @return index of a room in the arraylist.
	 */
	public int randRoom() { // Function to randomly generate a room of the building.
		Random randGen = new Random(); // creates new variable
		return randGen.nextInt(allRooms.size()); // finds random number
	}

	/**
	 * Function to return the Building string.
	 * 
	 * @return A string of the building size and the room info.
	 */
	public String getBString() {
		String s = "";
		s = xSize * 5 + " " + ySize * 5 + ";";
		for (Room r : allRooms) // for all rooms
			s = s + r.returnRString(); // add the room info
		return s;
	}

	/**
	 * A function to return the person in an array.
	 * 
	 * @param index The index
	 * @return A person at that index
	 */
	public Person getPerson(int index) {
		return people.get(index);
	}

	/**
	 * Function to update the metrics of all the people, by a set amount
	 */
	public void updatePMetrics() {
		for (Person p : people) { // for all people
			p.chngStat("Hunger", -0.2); // change Hunger by -0.2
			p.chngStat("Thirst", -0.1); // change Thirst by -0.1
			p.chngStat("Sleep", -0.05); // change Sleep by -0.05
		}
	}

	/**
	 * Function to add a path to a person, to get them to an object that will
	 * replenish one of their stats, or change it if they're there.
	 * 
	 * @param P    Person to have their stats change
	 * @param stat The stat that will be changed
	 * @return A boolean to determine if a path has been set/a stat has been changed
	 */
	public boolean addStatPath(Person P, String stat) {
		Room rm = allRooms.get(roomLoc(P)); // Gets the person's room
		boolean flag = false;
		for (Thing O : allObj) {// for all Objects
			if (flag == false) {
				Room objRoom = allRooms.get(roomLoc(O)); // Room the object is in
				if (O.name == "Bed" && stat == "Sleep") { // if the object is a bed, and the stat to be changed is
															// sleep.
					flag = true;
					if ((P.getPos().x == O.getPos().x) && (P.getPos().y == O.getPos().y)) { // if the person is on the
																							// bed
						P.chngStat("Sleep", 100); // increase their sleep stat by 100
					} else {
						statPaths(P, rm, objRoom, O); // add the path to the object, to the person
					}
				} else if (O.name == "Sink" && stat == "Thirst") { // if the object is a sink and the stat to be changed
																	// is thirst.
					flag = true;
					if ((P.getPos().x == O.getPos().x) && (P.getPos().y == O.getPos().y)) { // as above
						P.chngStat("Thirst", 100); // increase their thirst stat by 100
					} else {
						statPaths(P, rm, objRoom, O);// as above
					}
				} else if (O.name == "Fridge" && stat == "Hunger") { // if the object is a fridge and the stat to be
																		// changed is hunger.
					flag = true;
					if ((P.getPos().x == O.getPos().x) && (P.getPos().y == O.getPos().y)) { // as above
						P.chngStat("Hunger", 100); // increase their hunger stat by 100
					} else {
						statPaths(P, rm, objRoom, O); // as above.
					}
				}
			}
		}
		return flag;
	}

	/**
	 * Function to add to a person, the path to an object that will replenish their
	 * stats.
	 * 
	 * @param P     The person to have a path added to them
	 * @param curr  the room the person is currently in
	 * @param objRm the room of the object
	 * @param O     the object
	 */
	private void statPaths(Person P, Room curr, Room objRm, Thing O) {
		P.addPathPt(curr.returnDoor(-1)); // go to just before the door of the current room
		P.addPathPt(curr.returnDoor(0)); // go to the door of current room
		P.addPathPt(objRm.returnDoor(1)); // go to just outside the door of the object's room
		P.addPathPt(objRm.returnDoor(-1)); // go to just inside the room of the object
		P.addPathPt(O.getPos()); // go to the object.
	}

}
