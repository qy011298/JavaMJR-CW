// Interface implemented by all Actions the player can take
public interface Action {
    // Execute the Action on the World.
    public void execute(World w);
}

