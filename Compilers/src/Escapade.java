import java.util.List;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.PushbackReader;
import java.io.StringReader;

public class Escapade {

    public static void main(String[] args) {
        // Print welcome message.
        System.out.println("Welcome to Escapade.");

        //PushbackInputStream input = new PushbackInputStream(System.in);

        // Set up standard input to be read a line at a time.
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        // Create the game world.
        World w = new World();

        // Describe the starting room.
        new Look().execute(w);

        // Create a parser for handling input.
        // CHANGE THIS TO INSTANTIATE YOUR OWN LINEPARSER CLASS.
        LineParser p = new StaticParser();

        // Keep processing input from the player.
        while (true) {

            // Prompt for a command and read it.

            System.out.println("What do you want to do?");
            String line = null;
            
            try {
                line = input.readLine();
            }
            catch (java.io.IOException e) {
                System.err.println("Error reading from standard input.");
                System.exit(1);
            }
            
            // Quit if standard input was closed.
            if (line == null) {
                break;
            }
            

            // Echo back the input.
            // Useful for running the game against input stored in a file.
            System.out.println("> " + line);
            System.out.println("");

            // Parse and process the input.
            {
                PushbackReader lineStream = new PushbackReader(new StringReader(line));
                List<Action> actions = p.parse(lineStream);
                if (actions == null) {
                    System.out.println("I didn't understand your command.");
                }
                else {
                    for (Action a : actions) {
                        a.execute(w);
                    }
                }
            }
        }
        
        System.out.println("Goodbye.");
        return;
    }

}

