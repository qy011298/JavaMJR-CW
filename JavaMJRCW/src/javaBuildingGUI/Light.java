package javaBuildingGUI;

import java.awt.Point;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 * Class to hold all the unique functions for the light. Inherited from "thing"
 * 
 * @author Will
 *
 */
public class Light extends Thing {

	private Paint P;

	/**
	 * Constructor for the light object.
	 * 
	 * @param x      Point for the light's location
	 * @param objAmt The object number when the light was made. used as the ID.
	 */
	Light(Point x) {
		super(x);
		name = "Light";
		setColour();
		setDimen(10, 10);
	}

	/**
	 * Private function to set the colour
	 */
	private void setColour() {
		P = Color.YELLOW;
	}

	/**
	 * Method overriding to use the correct "drawThing" method from the GUI.
	 */
	public void show(GUI g) {
		g.drawThing(pos.x, pos.y, P, wdth, hght);
	}
}
