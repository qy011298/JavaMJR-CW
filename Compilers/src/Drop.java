// The "drop" action.
public class Drop implements Action {
    String item;

    public Drop(String item) {
        this.item = item;
    }

    public void execute(World w) {
        // Check the player is carrying the Item.
        if (!w.getLoc(item).equals("player")) {
            System.out.println("You aren't carrying that.");
            System.out.println();
            return;
        }
        // Move the Item to the player's Room.
        String hereId = w.getLoc("player");
        w.setLoc(this.item, hereId);
        System.out.println("Dropped.");
        System.out.println();
    }
}

