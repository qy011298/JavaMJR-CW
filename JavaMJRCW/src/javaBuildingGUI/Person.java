package javaBuildingGUI;

import java.awt.Point;
import java.util.ArrayList;

/**
 * Class for the person object that inherited the Thing
 * 
 * @author Will
 *
 */
public class Person extends Thing {

	private double sLevel = 50.0; // Sleep Level
	private double hLevel = 50.0; // Hunger Level
	private double tLevel = 50.0; // Tiredness Level
	private ArrayList<Point> Path; // arraylist of stored points for the path

	/**
	 * Constructor for the person
	 * 
	 * @param x The point that the person is spawn in
	 */
	Person(Point x) {
		super(x);
	}

	/**
	 * Method override for showing the person.
	 */
	public void show(GUI g) {
		g.drawPerson(pos.x * 5, pos.y * 5);
	}

	/**
	 * Function to clear the person's path
	 */
	public void clearPath() { // if the path has any points, clear it.
		if (Path.size() > 0) {
			Path.clear();
		}
	}

	/**
	 * Function to add a point to the person's path
	 * 
	 * @param x the point to be added to the path
	 */
	public void addPathPt(Point x) { // adds another point to the array
		Path.add(x);
	}

	/**
	 * Function to remove the path point
	 */
	public void remvPathPt() { // removes the first point in the array
		Path.remove(0); // puts every other point to the left.
	}

	/**
	 * Function to initialise the variable
	 */
	public void createPathvar() {
		Path = new ArrayList<Point>(); // function to create the variable
	}

	/**
	 * Getter to return the path size of the persons.
	 * 
	 * @return integer for how big the path arraylist is.
	 */
	public int getPathSize() { // returns the size of the Path array
		return Path.size();
	}

	/**
	 * Getter to find the next point in the person's point
	 * 
	 * @return the next point in the person's path
	 */
	public Point nextPoint() {
		return Path.get(0);
	}

	/**
	 * Function to move the person to the next path point
	 * 
	 * @return a boolean value that returns 0 if still moving, -1 if not moving
	 */
	public int movePerson() { // Moves the person by reading the path
		int dx, dy, xP, yP;
		if (Path.size() > 0) {
			xP = Path.get(0).x; // Gets the x coordinate of the next destination
			yP = Path.get(0).y; // Gets the y one
			dx = xP - pos.x; // finds the difference in each coord and current position
			dy = yP - pos.y;
			if ((dx == 0) && (dy == 0)) { // if there's no difference (got to the location)
				remvPathPt(); // remove that path point
				if (Path.size() == 0)
					return -1; // if no more points, return -1
				return 0; // otherwise, return 0
			} else {
				if (dx > 1) // Code to reduce dx and dy so it moves incrementally
					dx = 1;
				else if (dx < -1)
					dx = -1;
				if (dy > 1)
					dy = 1;
				else if (dy < -1)
					dy = -1;
				pos.translate(dx, dy); // translate it
				return 0; // returns 0;
			}

		}
		return -1;
	}

	/**
	 * Getter to return the sleep stat of the person
	 * 
	 * @return the sleep stat
	 */
	public double getSleepStat() {
		return sLevel;
	}

	/**
	 * Getter to return the hunger stat of the person
	 * 
	 * @return The hunger stat
	 */
	public double getHungStat() {
		return hLevel;
	}

	/**
	 * Getter to return the thirst stat
	 * 
	 * @return The thirst stat
	 */
	public double getThirStat() {
		return tLevel;
	}

	/**
	 * A tostring method for the info boxes.
	 */
	public String toString() {
		String s;
		s = " is at " + pos.x + "," + pos.y + ".\n Sleep: " + String.format("%.2f", sLevel);
		if (sLevel <= 0)
			s = s + " !";
		s = s + "\n Hunger: " + String.format("%.2f", hLevel);
		if (hLevel <= 0)
			s = s + " !";
		s = s + "\n Thirst: " + String.format("%.2f", tLevel);
		if (tLevel <= 0)
			s = s + " !";
		return s;
	}

	/**
	 * Function to change the stats of the person
	 * 
	 * @param stat  The stat that is due to be changed
	 * @param value The value it will be changed by.
	 */
	public void chngStat(String stat, double value) {
		if (stat == "Hunger" && (hLevel > 0 || (value > 0)))
			hLevel = hLevel + value;
		else if (stat == "Thirst" && (tLevel > 0 || (value > 0)))
			tLevel = tLevel + value;
		else if (stat == "Sleep" && (sLevel > 0 || (value > 0)))
			sLevel = sLevel + value;
	}
}
