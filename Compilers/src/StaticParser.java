import java.util.List;
import java.util.LinkedList;
import java.util.Collections;
import java.io.PushbackReader;

// This dummy implementation of LineParser returns a fixed sequence of commands.
public class StaticParser implements LineParser {
    LinkedList<Action> actions;

    public StaticParser() {
        this.actions = new LinkedList<Action>();
        actions.add(new Examine("chair"));
        actions.add(new Take("chair"));
        actions.add(new Go("north"));
        actions.add(new Drop("chair"));
        actions.add(new Look());
    }
    
    public List<Action> parse(PushbackReader r) {
        if (actions.isEmpty()) {
            return Collections.singletonList(new Quit());
        }
        else {
            return Collections.singletonList(actions.remove());
        }
    }
}

