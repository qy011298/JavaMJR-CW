// An Item in the World.
public class Item {
    // The World that contains this Item.
    private World world;
    
    // The ID of the Room containing the Item ("player" if carried).
    private String loc;
    
    // The Item's unique ID.
    private String id;
    
    // A phrase such as "a"/"the"/"some" that precedes the Item's name in text.
    private String article;
    
    // The Item's name, as usually displayed in text.
    private String name;
    
    // A description of the Item, displayed when it is examined.
    private String desc;
    
    public Item(World world, String loc, String id, String article, String name, String desc) {
        // The constructor just saves all the parameters passed.
        this.world = world;
        this.loc = loc;
        this.id = id;
        this.article = article;
        this.name = name;
        this.desc = desc;
    }

    // Most methods are simple accessors.

    public String getId() {
        return this.id;
    }
    
    public String getLoc() {
        return this.loc;
    }
    
    public void setLoc(String loc) {
        this.loc = loc;
    }
    
    public String getName() {
        return this.name;
    }

    // Return the concatenation of the Item's article and name.
    // For example, "a chair", rather than just "chair".
    public String getTheName() {
        return this.article + " " + this.name;
    }
    
    public String getDesc() {
        return this.desc;
    }
}

