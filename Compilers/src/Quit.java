// The "quit" action.
public class Quit implements Action {
    public Quit () {
    }
    
    public void execute(World w) {
        System.out.println("Goodbye.");
        System.exit(0);
    }
}

