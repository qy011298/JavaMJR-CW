package javaBuildingGUI;

import java.awt.Point;

/**
 * A class to represent the fridge object. Inherited from the "Thing Class"
 * 
 * @author Will
 *
 */
public class Fridge extends Thing {
	/**
	 * Constructor for the fridge
	 * 
	 * @param x  point the fridge will be placed.
	 * @param id The number of objects
	 */
	Fridge(Point x) {
		super(x);
		setImage("fridge.png"); // calls the image
		setDimen(30, 30); // sets the dimensions
		name = "Fridge"; // sets the name
	}

}
