package javaBuildingGUI;

import java.awt.Point;

/**
 * Class containing the Table constructor. inherited from the Thing Class
 * 
 * @author Will
 *
 */
public class Table extends Thing {
	/**
	 * Constructor for the table.
	 * 
	 * @param x  Point the table is placed
	 * @param id id the table is given
	 */
	Table(Point x) {
		super(x);
		setImage("table.jpg");
		setDimen(40, 30);
		name = "Table";
	}
}
