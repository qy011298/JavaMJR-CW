// The "examine" action.
public class Examine implements Action {
    String item;

    public Examine(String item) {
        this.item = item;
    }

    public void execute(World w) {
        // Find the player's location.
        String hereId = w.getLoc("player");
        // Check the item to be examined is here.
        if (!w.getLoc(item).equals(hereId)) {
            System.out.println("You can't see that.");
            System.out.println();
            return;
        }
        // Print its description.
        System.out.println(w.lookupItem(this.item).getDesc());
        System.out.println();
    }
}

