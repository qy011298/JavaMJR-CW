import java.util.TreeMap;
import java.util.Map;
import java.util.HashSet;

// A Room within the game World.
// Rooms can contain Items and the player.
// They have exits linking to other Rooms.
public class Room {
    // The World that contains this Room.
    private World world;

    // A map of exits, from a direction name to the destination Room.
    // Use a TreeMap, not a HashMap, so the ordering is consistent.
    private TreeMap<String, Room> exits;
    
    // A unique ID for the Room.
    private String id;
    
    // The Room's name, displayed at the top of "look" output.
    private String name;
    
    // A description of the room, displayed in the middle of "look" output.
    private String desc;

    public Room(World world, String id, String name, String desc) {
        // Initialise the exit map and save the parameters.
        this.exits = new TreeMap<String, Room>();
        this.world = world;
        this.id = id;
        this.name = name;
        this.desc = desc;
    }

    // The following three methods (joinRoom, addItem, movePlayer) all return
    // the current Room. This allows them to be chained together easily to
    // create the game world in World's constructor.

    // Add a new Room to the World, adjacent to this one in direction "dir".
    // The new Room is given an exit in the opposite direction back to this one.
    // Returns the new Room.
    public Room joinRoom(String dir, String id, String name, String desc) {
        Room dest = world.addRoom(id, name, desc);
        this.setExit(dir, dest);
        dest.setExit(reverse(dir), this);
        return dest;
    }

    // Add a new Item to the World in this Room.
    // Returns the Room.
    public Room addItem(String id, String article, String name, String desc) {
        this.world.addItem(this.id, id, article, name, desc);
        return this;
    }

    // Move the player into this Room.
    // Returns the Room.
    public Room movePlayer() {
        this.world.setLoc("player", this.id);
        return this;
    }

    // Accessor for Room's ID.
    public String getId() {
        return this.id;
    }

    // Accessor for Room's name.
    public String getName() {
        return this.name;
    }

    // Print a description of the room in response to "look".
    public void describe() {
        // Print the room's name and description.
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("You are " + this.name + ".");
        System.out.println("");
        System.out.println(this.desc);
        System.out.println("");

        // Summarise the directions and destinations of exits.
        System.out.println("Exits lead: ");
        if (this.exits.size() > 0) {
            for (Map.Entry<String, Room> exit : exits.entrySet()) {
                System.out.println(exit.getKey() + "\t: " + exit.getValue().getName());
            }
        }
        System.out.println("");

        // List any Items in the room.
        HashSet<Item> contents = this.world.roomContents(this.id);
        if (contents.size() > 0) {
            System.out.println("You can also see: ");
            for (Item item : contents) {
                System.out.println(item.getTheName());
            }
            System.out.println("");
        }

        // List any Items held by the player.
        HashSet<Item> inventory = this.world.roomContents("player");
        if (inventory.size() > 0) {
            System.out.println("You are carrying: ");
            for (Item item : inventory) {
                System.out.println(item.getTheName());
            }
            System.out.println("");
        }

    }

    // Given an object's ID, return whether it is in this Room.
    public boolean contains(String objId) {
        return this.id.equals(world.getLoc(objId));
    }

    // Return the Room in direction "dir" from this Room.
    public Room exit(String dir) {
        return this.exits.get(dir);
    }

    // Add an exit from this Room in direction "dir" to Room "dest".
    // Do not automatically add an exit back in the reverse direction.
    public void setExit(String dir, Room dest) {
        this.exits.put(dir, dest);
    }

    // Given a compass direction, return its reverse.
    public String reverse(String dir) {
        switch(dir) {
            case "north":
                return "south";
            case "east":
                return "west";
            case "south":
                return "north";
            case "west":
                return "east";
            default:
                break;
        }
        assert(false);
        return "";
    }
}

