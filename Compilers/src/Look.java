// The "look" action.
public class Look implements Action {
    public Look () {
    }
    
    public void execute(World w) {
        // Get the player's location.
        String hereId = w.getLoc("player");
        Room here = w.lookupRoom(hereId);
        // Describe it.
        here.describe();
    }
}

