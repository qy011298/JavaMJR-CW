// The "take" action.
public class Take implements Action {
    String item;

    public Take(String item) {
        this.item = item;
    }

    public void execute(World w) {
        // Check the Item is in the same Room as the player.
        String hereId = w.getLoc("player");
        if (!w.getLoc(item).equals(hereId)) {
            System.out.println("You can't see that.");
            System.out.println();
            return;
        }
        // Move the Item to the player.
        w.setLoc(this.item, "player");
        System.out.println("Taken.");
        System.out.println();
    }
}

