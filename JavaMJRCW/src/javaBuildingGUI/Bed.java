package javaBuildingGUI;

import java.awt.Point;

/**
 * Class for the Bed, inherited from the "Thing" Class
 * 
 * @author Will
 *
 */
public class Bed extends Thing {
	/**
	 * Constructor for Bed
	 * 
	 * @param x  Point the bed is generated.
	 * @param id The ID the bed gets.
	 */
	Bed(Point x) {
		super(x);
		setImage("Bed.png"); // Calls setImage from the Thing class, with the correct string.
		setDimen(30, 40); // Calls setDimen from thing class, with dimensions 30 by 40.
		name = "Bed";
	}
}
