package javaBuildingGUI;

import java.awt.Point;
import javafx.scene.image.Image;

/**
 * Class containing the Thing functions.
 * 
 * @author Will
 *
 */
abstract class Thing {

	Point pos;
	protected int wdth, hght;
	protected Image img;
	protected String name = "null";
	protected int ID;
	protected double sizeX, sizeY;
	static int id = 0;

	/**
	 * Constructor for a Thing
	 * 
	 * @param x Point that the thing is spawned.
	 */
	Thing(Point x) {
		setPos(x);
		setID(id++);
	}

	/**
	 * Getter for the thing ID
	 * 
	 * @return returns the ID as an int.
	 */
	public int getID() {
		return ID;
	}

	/**
	 * Getter for the position of the thing
	 * 
	 * @return the point the thing is.
	 */
	public Point getPos() {
		return pos;
	}

	/**
	 * A function to set the ID of the Thing
	 * 
	 * @param id The id that the object is set
	 */
	public void setID(int id) {
		ID = id;
	}

	/**
	 * Setter to set the position of the object
	 * 
	 * @param x The point the object is going to be set
	 */
	public void setPos(Point x) {
		pos = x;
	}

	/**
	 * Setter for the image of the object.
	 * 
	 * @param x The string that refers to the image
	 */
	protected void setImage(String x) {
		img = new Image(getClass().getResourceAsStream(x));
	}

	/**
	 * A function to place the information for the Thing, into a string
	 */
	public String toString() {
		return name + " is in " + pos.x + "," + pos.y + ".";
	}

	/**
	 * The show function for the Thing to be shown in the GUI
	 * 
	 * @param g The GUI that the thing is shown.
	 */
	public void show(GUI g) {
		g.drawThing(pos.x, pos.y, img, sizeX, sizeY);
	}

	/**
	 * Getter for the name of the thing.
	 * 
	 * @return A string for the name of the thing.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Function to set the dimensions for when the image is drawn.
	 * 
	 * @param x The size in the X direction
	 * @param y The size in the Y direction
	 */
	protected void setDimen(int x, int y) {
		wdth = x;
		hght = y;
		sizeX = x;
		sizeY = y;
	}

}
