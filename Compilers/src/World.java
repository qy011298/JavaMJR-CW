import java.util.HashMap;
import java.util.HashSet;

// The World stores all information to do with state of the game.
// The World's state consists of a set of Rooms and Items,
// plus the player's location.
// All Rooms and Items created must have a unique string ID,
// which is registered with the World.
public class World {
    // A map from item IDs to Items, storing all Items in the game.
    private HashMap<String, Item> items;
    // A map from room IDs to Items, storing all Rooms in the game.
    private HashMap<String, Room> rooms;
    // The room ID of the player's current location.
    private String playerLoc;
    
    public World() {
        // Initialise the HashMaps.
        this.items = new HashMap<String, Item>();
        this.rooms = new HashMap<String, Room>();

        // Set up most of the game world.
        this.addRoom(	        "home",         "sitting at home", "A modern house, probably built in the last ten years.")
            .movePlayer()
            .addItem(		"chair",	"a", "green wooden chair", "An ordinary wooden chair. No time to rest, though.")
            .addItem(		"bed",		"your", "wooden bed", "This is no time to sleep.")
            .joinRoom("north",	"road",	        "standing on a road", "A straight, concrete road, that leads north/south.")
            .joinRoom("north",  "forest1",      "lost in a forest", "In all directions, you can see only trees.")
            .joinRoom("east",   "forest2",      "lost in a forest", "In all directions, you can see only trees.")
            .joinRoom("south",  "forest3",      "lost in a forest", "In all directions, you can see only trees.")
            .joinRoom("west",   "forest4",      "lost in a forest", "In all directions, you can see only trees.")
            .addItem(           "treasure",     "the", "treasure", "Gold and sparkling jewels.")
            ;

        // Add the bookshop.
        this.lookupRoom("road")
            .joinRoom("west",   "bookshop",     "in a bookshop", "Dusty textbooks line the shelves.")
            .addItem(           "book",         "an", "old green book", "It's about programming in COBOL.")
            ;

        // Add dummy exits in the forest maze.
        Room forest = this.lookupRoom("forest1");
        this.lookupRoom("forest1").setExit("west", forest);
        this.lookupRoom("forest1").setExit("north", forest);
        this.lookupRoom("forest2").setExit("north", forest);
        this.lookupRoom("forest2").setExit("east", forest);
        this.lookupRoom("forest3").setExit("east", forest);
        this.lookupRoom("forest3").setExit("south", forest);
        this.lookupRoom("forest4").setExit("north", forest);
        this.lookupRoom("forest4").setExit("south", forest);
        this.lookupRoom("forest4").setExit("west", forest);
    }

    // Given an object's ID, return the ID of its location (a Room).
    public String getLoc(String id) {
        // Treat "player" as a request for the player's location.
        if (id.equals("player")) {
            return this.playerLoc;
        }
        // Otherwise, it's an Item.
        else {
            return items.get(id).getLoc();
        }
    }

    // Given an object's ID and a Room ID, move the object into the room.
    public void setLoc(String id, String dest) {
        // Treat "player" as a request to move the player.
        if (id.equals("player")) {
            this.playerLoc = dest;
        }
        // Otherwise, move an Item.
        else {
            this.lookupItem(id).setLoc(dest);
        }

        // If the Item with ID treasure is moved to the Room with the ID home,
        // end the game.
        if (id.equals("treasure") && dest.equals("home")) {
            System.out.println("You got the treasure safely home!");
            System.exit(0);
        }
        
    }

    // Given a Room's ID, return a HashSet containing all Items in the Room.
    public HashSet<Item> roomContents(String id) {
        HashSet<Item> contents = new HashSet<Item>();
        for (HashMap.Entry<String, Item> item : this.items.entrySet()) {
            if (id.equals((item.getValue().getLoc()))) {
                contents.add(item.getValue());
            }
        }
        return contents;
    }

    // Add a Room to the world, creating the object and registering it.
    // Return the Room created.
    public Room addRoom(String id, String name, String desc) {
        Room r = new Room(this, id, name, desc);
        this.rooms.put(id, r);
        return r;
    }

    // Add an Item to the world, creating the object and registering it.
    // Return the Item created.
    public Item addItem(String loc, String id, String article, String name, String desc) {
        Item i = new Item(this, loc, id, article, name, desc);
        this.items.put(id, i);
        return i;
    }

    // Given a Room's ID, return the Room.
    public Room lookupRoom(String id) {
        return this.rooms.get(id);
    }

    // Given an Item's ID, return the Item.
    public Item lookupItem(String id) {
        return this.items.get(id);
    }
    
}

