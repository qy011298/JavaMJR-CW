package javaBuildingGUI;

import java.awt.Point;
import java.util.Random;

/**
 * Class that holds the functions of all the rooms.
 * 
 * @author Will
 *
 */
public class Room {

	private String[] coOrds; // array that stores the split string
	private int x1, x2, y1, y2, xd, yd, dSize; // The variables storing the coordinates for the room.

	/**
	 * Constructor for the Room
	 * 
	 * @param s The Room string that builds the room.
	 */
	Room(String s) {
		coOrds = s.split(" "); // split the strings
		int[] intcoOrds = { 0, 0, 0, 0, 0, 0, 0 }; // Array that temporarily stores the split string
		for (int x = 0; x < coOrds.length; x++) {
			intcoOrds[x] = Integer.parseInt(coOrds[x]);
		}
		x1 = intcoOrds[0]; // Storing the integers for the dimensions of the room
		x2 = intcoOrds[2];
		y1 = intcoOrds[1];
		y2 = intcoOrds[3];
		xd = intcoOrds[4]; // xd and yd refer to the coordinates of the door
		yd = intcoOrds[5];
		if (intcoOrds[6] == 0)
			dSize = 1;
		else
			dSize = intcoOrds[6]; // if the size is specified. if not, it's size 1.
	}

	/**
	 * toString function for the info boxes in the GUI
	 */
	public String toString() { // simple function to print out info about the room
		String res = "";
		res = res + "Room from (" + x1 + "," + y1 + ") to (" + x2 + "," + y2 + ") Door at (" + xd + "," + yd + ") "
				+ " size:" + dSize;
		return res;
	}

	/**
	 * Function to return the Room string
	 * 
	 * @return the string with the room info
	 */
	public String returnRString() {
		String rm = "";
		rm = x1 * 5 + " " + y1 * 5 + " " + x2 * 5 + " " + y2 * 5 + " " + xd * 5 + " " + yd * 5 + ";";
		return rm;
	}

	/**
	 * Function to determine if a point is situated within a room.
	 * 
	 * @param P The point that may or may not be in the room
	 * @return A true or false value, depending on if it's in the room
	 */
	public boolean isInRoom(Point P) {
		int tX, tY; // Temporary x and temporary y
		tX = (int) P.getX(); // finds the x and y coordinate of the person
		tY = (int) P.getY();

		if ((tX >= x1) && (tX <= x2) && (tY >= y1) && (tY <= y2)) { // if the points fall in the correct range
			return true;
		} else
			return false;
	}

	/**
	 * Function to show the room
	 * 
	 * @param g   The GUI that the room is being shown on.
	 * @param rNo The room's number
	 */
	public void show(GUI g, int rNo) {
		g.DrawRoom(x1 * 5, y1 * 5, x2 * 5, y2 * 5, xd * 5, yd * 5, rNo);
	}

	/**
	 * A function to get a random point within the room.
	 * 
	 * @param O The random object passed to the function
	 * @return The point in the room.
	 */
	public Point getRandom(Random O) { // makes 2 random integers and puts it into a point
		int valx, valy;
		valx = x1 + 10 + O.nextInt(x2 - x1 - 10); // A random X and Y within the room
		valy = y1 + 10 + O.nextInt(y2 - y1 - 10);

		Point P = new Point(valx, valy); // Making the point
		return P;
	}

	/**
	 * Getter to get the height of the room
	 * 
	 * @return The heigh of the room as an int.
	 */
	public int getHeight() {
		return (y2 - y1);
	}

	/**
	 * Getter to get the width of the room.
	 * 
	 * @return The width of the room as an int.
	 */
	public int getWidth() {
		return (x2 - x1);
	}

	/**
	 * Function to return a point near the Top left corner of the room.
	 * 
	 * @return A point near the top left corner of the room
	 */
	public Point getTLCorner() {
		int valx, valy;
		valx = x1 + 5; // The coordinate + 2 (to no intefere with the walls thickness)
		valy = y1 + 5;
		Point P = new Point(valx, valy);
		return P;
	}

	/**
	 * Function to get the middle point of the room.
	 * 
	 * @return The middle point of the room.
	 */
	public Point getMid() {
		int valx, valy;
		valx = x1 + (x2 - x1) / 2; // Finding the x and Y coordinates
		valy = y1 + (y2 - y1) / 2;
		Point P = new Point(valx, valy);
		return P;
	}

	/**
	 * Function to return a point in relation to the door.
	 * 
	 * @param loc The rule to determine the point that is in relation to the door.
	 * @return The point that is in relation to the door
	 */
	public Point returnDoor(int loc) { // Function to return coordinates of the door and around it.
		Point Q = new Point(xd, yd);
		if (xd == x1) { // if door is on left wall
			if (loc == 1) { // Outside door
				Point p = new Point(xd - 5, yd);
				return p; // returns the point just outside the door
			} else if (loc == -1) { // inside door
				Point p = new Point(xd + 5, yd);
				return p; // Returns the point just inside the door
			} else
				return Q; // Returns the door point
		} else if (xd == x2) { // if door is on right wall
			if (loc == 1) { // Outside door
				Point p = new Point(xd + 5, yd);
				return p;
			} else if (loc == -1) { // Inside door
				Point p = new Point(xd - 5, yd);
				return p;
			} else
				return Q;
		} else if (yd == y1) { // if door is on top wall
			if (loc == 1) { // Outside door
				Point p = new Point(xd, yd - 5);
				return p;
			} else if (loc == -1) { // inside door
				Point p = new Point(xd, yd + 5);
				return p;
			} else
				return Q;
		} else if (yd == y2) { // if door is on bottom wall
			if (loc == 1) { // outside door
				Point p = new Point(xd, yd + 5);
				return p;
			} else if (loc == -1) { // inside door
				Point p = new Point(xd, yd - 5);
				return p;
			} else
				return Q;
		}
		return Q;
	}
}
