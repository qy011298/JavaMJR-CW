import java.util.List;

import org.antlr.v4.tool.Grammar;

import java.io.PushbackReader;


// Interface implemented by all parsers of lines of player input.
public interface LineParser {
    // Take a line of input; return a list of parsed actions.
    public List<Action> parse(PushbackReader r);
    
}



