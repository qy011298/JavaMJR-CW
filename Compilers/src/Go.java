// The "go" action.
public class Go implements Action {
    // The direction to move in.
    String dir;

    public Go(String dir) {
        this.dir = dir;
    }

    public void execute(World w) {
        // Find where the player is.
        String hereId = w.getLoc("player");
        Room here = w.lookupRoom(hereId);
        // Find the exit in the specified direction.
        Room dest = here.exit(this.dir);
        // Check an exit exists.
        if (dest == null) {
            System.out.println("You can't go that way.");
            return;
        }
        // Move the player and automatically "look".
        w.setLoc("player", dest.getId());
        dest.describe();
    }
}

