package javaBuildingGUI;

import java.awt.Point;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

/**
 * The Class that contains all the functions that runs the GUI.
 * 
 * @author Will
 *
 */
public class GUI extends Application {
	private int canvasSize = 512; // Canvas size
	private Canvas canvas = new Canvas(canvasSize, canvasSize); // creating the overall scene
	private Canvas tgCanvas = new Canvas(300, 150); // the canvas for the thirst graph
	private Canvas hgCanvas = new Canvas(300, 150); // the canvas for the hunger graph
	private Canvas sgCanvas = new Canvas(300, 150); // the canvas for the sleep graph
	private GraphicsContext gc = canvas.getGraphicsContext2D(); // graphics context for the overall scene
	private GraphicsContext gcHGraph = hgCanvas.getGraphicsContext2D(); // graphics context for each graph
	private GraphicsContext gcTGraph = tgCanvas.getGraphicsContext2D();
	private GraphicsContext gcSGraph = sgCanvas.getGraphicsContext2D();
	private Building myBuilding; // the Building
	private VBox rtPane = new VBox(); // The vertical box that is in the right pane
	private int pplAmt = 0; // variable to store the amount of people wanting to be added
	private String objChoice; // the desired object choice
	private AnimationTimer personAnimation; // the animation
	private long animationSpeed = 40_000_000; // the animation speed, starting at 40_000_000
	private GraphPlot hGraph = new GraphPlot(gcHGraph, 300, 100); // New graph plots for all the graphs
	private GraphPlot sGraph = new GraphPlot(gcSGraph, 300, 100);
	private GraphPlot tGraph = new GraphPlot(gcTGraph, 300, 100);
	private ArrayList<ArrayList<Double>> hMetrics = new ArrayList<ArrayList<Double>>(); // Arraylists for all the data
																						// for the graphs
	private ArrayList<ArrayList<Double>> sMetrics = new ArrayList<ArrayList<Double>>();
	private ArrayList<ArrayList<Double>> tMetrics = new ArrayList<ArrayList<Double>>();
	private int gNum = 0; // The index of the person whose graphs are showing.
	private Boolean graphShown = false; // Start by not showing the graphs.

	/**
	 * Function to get the default building string
	 * 
	 * @return A string that will be used to construct the default building
	 */
	public String buildingString() {
		return "110 110;0 0 40 40 20 40;60 0 100 100 60 50;0 60 40 100 20 60";	//Returns default building
	}

	/**
	 * A function to load building strings from text files
	 * 
	 * @throws Exception to stop crashing if the file cannot be read
	 */
	public void loadBuilding() throws Exception { // In case the File reader doesn't work
		String bString = " ";
		JFileChooser chooser = new JFileChooser(); // creates a new file chooser
		chooser.setCurrentDirectory(new File(System.getProperty("user.home"))); // sets directory (for ease)
		int returnVal = chooser.showOpenDialog(null); // finds if file was selected or not
		if (returnVal == JFileChooser.APPROVE_OPTION) { // if file is selected
			File file = chooser.getSelectedFile(); // Puts it in a variable
			Scanner sLB = new Scanner(new FileReader(file));// scanner to read the file
			bString = sLB.nextLine(); // assigns the first string to variable b string
			myBuilding = new Building(bString); // Puts it as the active building
			myBuilding.addObject("Bed"); // adds a bed
			myBuilding.addObject("Fridge"); // adds a fridge
			myBuilding.addObject("Sink"); // adds a sink
			sLB.close(); // close the scanner.
		}
	}

	/**
	 * Function to initialise the scene, and determine backgrounds
	 */
	private void initBoard() {
		gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight()); // clear the scene
		gc.setFill(Color.LIGHTGRAY);
		gc.fillRect(0, 0, canvasSize, canvasSize); // draw a light grey background
		ArrayList<Thing> objList = myBuilding.getObjList(); // initialise the objectlist.
		for (int x = 0; x < myBuilding.getPrsnList().size(); x++) { // for all the people in building
			ArrayList<Double> a = new ArrayList<Double>(); // new arraylist added to each metric arraylist
			ArrayList<Double> b = new ArrayList<Double>();
			ArrayList<Double> c = new ArrayList<Double>();
			hMetrics.add(a);
			tMetrics.add(b);
			sMetrics.add(c);
			Person p = myBuilding.getPrsnList().get(x); // the person whose index this is
			int rNo = myBuilding.roomLoc(p); // location of the person
			if (rNo != -1) { // if they're in a room
				int rWdth = myBuilding.getRoomList().get(rNo).getWidth() * 5; // find room width
				int rHght = myBuilding.getRoomList().get(rNo).getHeight() * 5; // find room height
				Point TLCorner = new Point(myBuilding.getRoomList().get(rNo).getTLCorner()); // find TL corner
				for (Thing O : objList) { // for all objects
					if (rNo == myBuilding.roomLoc(O) && O.name == "Light") { // If there's a light in that room
						gc.setFill(Color.YELLOW);
						gc.fillRect((TLCorner.x * 5) - 20, (TLCorner.y * 5) - 20, rWdth, rHght); // draw a yellow
																									// background
					}
				}
			}
		}
	}

	/**
	 * Function to clear all the graphs.
	 */
	private void clearGraphs() {
		hGraph.clearGraph(0, 1000); // calling function from "GraphPlot"
		tGraph.clearGraph(0, 1000);
		sGraph.clearGraph(0, 1000);
	}

	/**
	 * Function to remove the graphs from the vertical box when not in use.
	 */
	private void rmvGraphCanvas() {
		rtPane.getChildren().remove(hgCanvas);
		rtPane.getChildren().remove(tgCanvas);
		rtPane.getChildren().remove(sgCanvas);
	}

	/**
	 * Function to add the graphs to the vertical box.
	 */
	private void addGraphCanvas() {
		rtPane.getChildren().add(hgCanvas);
		rtPane.getChildren().add(tgCanvas);
		rtPane.getChildren().add(sgCanvas);
	}

	/**
	 * Function to get an user input value from a dialogue box.
	 * 
	 * @param TStr      The title string
	 * @param currValue The current value in the dialogue box
	 * @return An integer that the user has input.
	 */
	private int getValue(String TStr, int currValue) {
		int ans = currValue;
		TextInputDialog dialog = new TextInputDialog(String.format("", currValue)); // Creates a new dialogue windox
		dialog.setTitle(TStr); // Set title
		dialog.setHeaderText(TStr); // Set header
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) // If a string is found
			try {
				ans = Integer.parseInt(result.get()); // change that string to an int and store it
			} catch (Exception e) { // In case the user attempt to enter a non-int
				showMessage("Error", "Invalid Input");
			}

		return ans;
	}

	/**
	 * Function to find the object choice of the User.
	 * 
	 * @param g The GUI that the function uses.
	 */
	private void getObjectChoice(GUI g) {
		ObservableList<String> data = FXCollections.observableArrayList("Bed", "Table", "Fridge", "Light", "Sink"); // make
																													// a
																													// list
																													// of
																													// these
																													// words.
		ComboBox<String> combo = new ComboBox<>(data); // create a new combo box
		Button btnAddObj = new Button("Add"); // create an "add" button
		Stage comboChoice = new Stage(); // make a new stage
		comboChoice.setTitle("Pick an Object"); // set title
		BorderPane bPane = new BorderPane(); // new borderPane
		Group root = new Group(); // new group
		combo.setValue(data.get(0)); // Set default value to index 0- bed.
		Scene comboScene = new Scene(bPane, 200, 75); // Make a new scene
		root.getChildren().addAll(combo); // add the combo box to the group
		bPane.setBottom(btnAddObj); // set the button to the bottom
		bPane.setTop(root); // set the root with the combo box in it, to the top
		BorderPane.setAlignment(btnAddObj, Pos.BOTTOM_RIGHT); // set the button allignment to the right
		comboChoice.setScene(comboScene); // set the scene
		comboChoice.show(); // show the scene
		btnAddObj.setOnAction(new EventHandler<ActionEvent>() { // When the button is pressed
			public void handle(ActionEvent event) {
				objChoice = combo.getValue(); // get the value in the combo box
				myBuilding.addObject(objChoice); // Add the object to the building
				myBuilding.Show(g); // Show the building.
			}
		});
	}

	/**
	 * Function to show an alert to the User.
	 * 
	 * @param TStr the Title string.
	 * @param CStr the content string.
	 */
	private void showMessage(String TStr, String CStr) {
		Alert alert = new Alert(AlertType.INFORMATION); // Makes a new alert
		alert.setTitle(TStr); // Sets it with the title.
		alert.setHeaderText(null); // No Header
		alert.setContentText(CStr); // Content of 2nd parameter string
		alert.showAndWait(); // Show and wait to be exited
	}

	/**
	 * Function to show the "About" message.
	 */
	private void showAbout() {
		showMessage("About", "Demo of Intelligent Building by William Parker"); // Shows Message
	}

	/**
	 * Function to show the "Help" message.
	 */
	private void showHelp() {
		showMessage("Help", "Please refer to the accompanying document for a brief user manual"); // Shows Message
	}

	/**
	 * A Horizontal box to set in all the buttons
	 * 
	 * @param g the GUI that the buttons will be added to.
	 * @return The HBox with all the buttons in it.
	 */
	HBox setButtons(GUI g) {
		HBox BBox = new HBox();
		Button btnAddPpl = new Button("Add Person"); // Add person button
		Button btnDraw = new Button("Draw"); // Draw Button
		Button btnStart = new Button("Start"); // Start Button
		Button btnPse = new Button("Pause"); // Pause Button
		Button btnAddObj = new Button("Add Object"); // Add Object Button
		Button btnSpdUP = new Button("Speed up"); // Speed up button
		Button btnSlwDw = new Button("Slow Down"); // Slow down button

		btnAddPpl.setOnAction(new EventHandler<ActionEvent>() { // On "Add Person" Click.
			public void handle(ActionEvent event) {
				pplAmt = getValue("Enter amount of people", pplAmt); // Find amount of people
				for (int i = 0; i < pplAmt; i++) { // Add that amount of people to building
					myBuilding.addPerson();
				}
				myBuilding.Show(g); // Show the GUI
			}
		});
		btnStart.setOnAction(new EventHandler<ActionEvent>() { // On "Start" button Click
			@Override
			public void handle(ActionEvent event) {
				for (int x = 0; x < myBuilding.getPAmt(); x++) { // Add a path to every person
					myBuilding.addPath(x);
				}
				personAnimation.start(); // start the animation
				btnStart.setDisable(true); // Disable buttons that aren't able to be used whilst animation is playing.
				btnAddPpl.setDisable(true);
				btnAddObj.setDisable(true);
				btnDraw.setDisable(true);
				btnSpdUP.setDisable(false); // Enable the animation speed controls.
				btnSlwDw.setDisable(false);
			}
		});
		btnPse.setOnAction(new EventHandler<ActionEvent>() { // On "Pause" button Click
			@Override
			public void handle(ActionEvent event) {
				personAnimation.stop(); // Stop the animation
				btnStart.setDisable(false); // Enable all of the buttons that can be used.
				btnAddPpl.setDisable(false);
				btnAddObj.setDisable(false);
				btnDraw.setDisable(false);
				btnSpdUP.setDisable(true); // disable the animation speed controls
				btnSlwDw.setDisable(true);
			}
		});
		btnAddObj.setOnAction(new EventHandler<ActionEvent>() { // On "Add Object" button click
			public void handle(ActionEvent event) {
				getObjectChoice(g); // Show the get object window
			}
		});
		btnDraw.setOnAction(new EventHandler<ActionEvent>() { // On "draw" button click
			public void handle(ActionEvent event) {
				myBuilding.Show(g); // Show the building
				btnStart.setDisable(false); // Enable the control buttons
				btnAddPpl.setDisable(false);
				btnPse.setDisable(false);
				btnAddObj.setDisable(false);
			}
		});
		btnSpdUP.setOnAction(new EventHandler<ActionEvent>() { // on "Speed up" button click
			public void handle(ActionEvent event) {
				if (animationSpeed > 0) // if the animation speed can be increased
					animationSpeed = animationSpeed - 15_000_000; // increase by 15_000_000;
			}
		});
		btnSlwDw.setOnAction(new EventHandler<ActionEvent>() { // on "slow down" button click
			public void handle(ActionEvent event) {
				if (animationSpeed < 60_000_000) // If the animation speed can be slowed
					animationSpeed = animationSpeed + 15_000_000; // Decrease by 15_000_000
			}
		});
		btnStart.setDisable(true); // Start the programme with all the buttons disabled, apart from "draw"
		btnAddPpl.setDisable(true);
		btnPse.setDisable(true);
		btnAddObj.setDisable(true);
		btnSpdUP.setDisable(true);
		btnSlwDw.setDisable(true);
		BBox.getChildren().addAll(btnDraw, btnStart, btnPse, btnAddPpl, btnAddObj, btnSpdUP, btnSlwDw); // Add to the
																										// HBox
		return BBox;
	}

	/**
	 * Function to draw each room of the building, onto the GUI.
	 * 
	 * @param x1  x coordinate of the top left corner
	 * @param y1  y coordinate of the top left corner
	 * @param x2  x coordinate of the bottom right corner
	 * @param y2  y coordinate of the bottom right corner
	 * @param xd  x coordinate of the door
	 * @param yd  y coordinate of the door
	 * @param rNo The room number
	 */
	public void DrawRoom(int x1, int y1, int x2, int y2, int xd, int yd, int rNo) {
		int wThicc = 4; // Setting the wall thickness
		int lblx = x1 + 15; // The coordinates of the x label
		int lbly = y1 + 25;
		gc.setFill(Color.CORNFLOWERBLUE); // Colour of the walls
		gc.fillRect(x1, y1, x2 - x1, wThicc); // Draws top wall
		gc.fillRect(x1, y1, wThicc, y2 - y1 + wThicc); // Draws left wall
		gc.fillRect(x2, y1, wThicc, y2 - y1 + wThicc); // Draws right wall
		gc.fillRect(x1 + wThicc, y2, x2 - x1, wThicc); // Draws bottom wall
		gc.setFill(Color.BLACK); // set colour to black
		gc.setGlobalAlpha(0.5); // set transparency to 50%
		gc.fillText("Room " + rNo, lblx, lbly); // Write the room label
		gc.setGlobalAlpha(1); // transparency to 0%
		gc.setFill(Color.LIGHTGREY); // set the same number
		gc.fillRect(xd, yd, 10, 10); // Clears the canvas where the doors are
	}

	/**
	 * Function to draw the person. (as an oval to represent it)
	 * 
	 * @param x The x coordinate of the oval
	 * @param y The y coordinate of the oval
	 */
	public void drawPerson(int x, int y) {
		gc.setFill(Color.BLACK); //
		gc.fillOval(x, y, 10, 10);
	}

	/**
	 * Function to draw a non-person object.
	 * 
	 * @param x    x coordinate of the object
	 * @param y    y coordinate of the object
	 * @param img  the image of the object
	 * @param wdth the width dimension of the object
	 * @param hght the height dimension of the object
	 */
	public void drawThing(int x, int y, Image img, double wdth, double hght) { // Actual Draw function
		gc.drawImage(img, (x * 5) - 15, (y * 5) - 15, wdth, hght);
	}

	/**
	 * An override function to draw the an object, as an oval.
	 * 
	 * @param x    x coordinate of the object
	 * @param y    y coordinate of the object
	 * @param p    the colour of the object
	 * @param wdth width of the object
	 * @param hght height of the object
	 */
	public void drawThing(int x, int y, Paint p, int wdth, int hght) {
		gc.setFill(p);
		gc.fillOval(x * 5, y * 5, wdth, hght);
	}

	/**
	 * A function to set the menu on the startup.
	 * 
	 * @return The menu bar for the GUI.
	 */
	MenuBar setMenu() {
		MenuBar menuBar = new MenuBar(); // Creating the menu bar
		Menu mHelp = new Menu("Help"); // Creating the Help Menu
		MenuItem mAbout = new MenuItem("About"); // Menu Item "About"
		MenuItem miHelp = new MenuItem("Help"); // Menu Item "Help"
		mHelp.getItems().addAll(mAbout, miHelp); // Add about and help items to the Help Menu
		Menu mFile = new Menu("File"); // Creating the File menu.
		MenuItem mExit = new MenuItem("Exit"); // Menu Item "Exit"
		MenuItem mLdBud = new MenuItem("Load Building"); // Menu Items for Loading Buildings
		mFile.getItems().addAll(mLdBud, mExit); // Add the load building function to the file menu,
												// and the exit
		menuBar.getMenus().addAll(mFile, mHelp); // Add menus to the menu bar

		mAbout.setOnAction(new EventHandler<ActionEvent>() { // On the "about" item action.
			@Override
			public void handle(ActionEvent actionEvent) {
				showAbout(); // Show About section
			}
		});

		miHelp.setOnAction(new EventHandler<ActionEvent>() { // on the "help" item action.
			@Override
			public void handle(ActionEvent actionEvent) {
				showHelp(); // Show Help section
			}
		});
		mExit.setOnAction(new EventHandler<ActionEvent>() { // on the "Exit" item action
			public void handle(ActionEvent t) {
				System.exit(0); // Exit the programme.
			}
		});
		mLdBud.setOnAction(new EventHandler<ActionEvent>() { // On the "Load Building 1" item action.
			public void handle(ActionEvent t) {
				initBoard(); // Initialise board
				try {
					loadBuilding();
				} catch (Exception e) {
					showMessage("Warning", "File not read");
				}
			}
		});
		return menuBar; // return the menu, so can be added
	}

	/**
	 * Function to update the graphs, after the metrics have been added.
	 */
	private void updateGraph() {
		clearGraphs();
		if (gNum != -1) {
			hGraph.yPlot("Hunger for Person " + (gNum + 1), hMetrics.get(gNum), 0);
			sGraph.yPlot("Sleep for Person " + (gNum + 1), sMetrics.get(gNum), 0);
			tGraph.yPlot("Thirst for Person " + (gNum + 1), tMetrics.get(gNum), 0);
		}
	}

	/**
	 * On starting the application.
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		myBuilding = new Building(buildingString()); // Start the application by loading the 1st building
		primaryStage.setTitle("Building GUI"); // Set Title
		BorderPane bp = new BorderPane(); // Create BP
		Group root = new Group(); // Create Group
		primaryStage.getIcons().add(new Image(GUI.class.getResourceAsStream("BuildingIcon.png"))); // changes the icon
		Scene scene = new Scene(bp, canvasSize * 1.7, canvasSize * 1.3); // Creating Scene of certain size
		scene.getStylesheets().add(getClass().getResource("metrodarktheme.css").toExternalForm()); // Setting the Theme.
		primaryStage.setScene(scene); // Set the scene
		root.getChildren().add(canvas); // adding canvas to root
		bp.setCenter(root); // Setting the bp centre to root
		bp.setTop(setMenu()); // Setting the Menu to the top
		bp.setBottom(setButtons(this)); // Set the buttons
		gc.setFill(Color.LIGHTGRAY);
		gc.fillRect(0, 0, canvasSize, canvasSize); // Set a background
		GUI that = this;
		Label SysLbl = new Label("");
		rtPane.getChildren().add(SysLbl); // The information label for the buildings
		bp.setRight(rtPane); // Set the pane to the right hand side
		primaryStage.show(); // Showing the stage.
		personAnimation = new AnimationTimer() { // The animation timer
			private long lastupdate = 0;
			public void handle(long now) {
				if (now - lastupdate >= animationSpeed) { // If enough time has passed (set by animation speed)
					initBoard(); // Initialise Board
					myBuilding.Show(that); // Show the building
					myBuilding.updatePMetrics(); // update the person metrics (make it go down)
					SysLbl.setText(myBuilding.toString()); // Set the information text of the right pane
					for (int x = 0; x < myBuilding.getPAmt(); x++) { // For all people in the building
						Person curPerson = myBuilding.getPerson(x);
						hMetrics.get(x).add(curPerson.getHungStat()); // Get the person's stat levels and add them to
																		// the appropriate arraylist
						sMetrics.get(x).add(curPerson.getSleepStat());
						tMetrics.get(x).add(curPerson.getThirStat());
						boolean flag = false; // flag variable to determine if there is an object to refill the person's
												// stat
						if (graphShown == true) { // if the graphs are currently shown
							updateGraph();
						}
						if (myBuilding.mPerson(curPerson) == -1) { // if the person has stopped moving (i.e., they have
																	// no more points in their path)
							if (curPerson.getThirStat() <= 0.0) { // if the person is thirsty
								flag = myBuilding.addStatPath(curPerson, "Thirst"); // add a path to an object to refill
																					// thirst (return true or false if
																					// there is an object)
							}
							if (curPerson.getHungStat() <= 0.0) { // if the person is hungry
								if (flag != true) { // if no other path has been added
									curPerson.clearPath();
									flag = myBuilding.addStatPath(curPerson, "Hunger"); // add a path to an object to
																						// refill hunger
								}
							}
							if (curPerson.getSleepStat() <= 0.0) { // if the person is sleepy
								if (flag != true) { // if no other path has been added
									curPerson.clearPath();
									flag = myBuilding.addStatPath(curPerson, "Sleep"); // add a path to an object to
																						// refill sleep
								}
							}
							if (flag == false) { // if no paths have been added (no objects to refill stats are added)
								myBuilding.addPath(x); // Add a random path to the person.
							}
							flag = false; // resetting the path
						}
					}
					lastupdate = now;
				}
			}
		};
		root.setOnMouseClicked(new EventHandler<MouseEvent>() { // when the canvas is clicked
			@Override
			public void handle(MouseEvent event) {
				for (int x = 0; x < myBuilding.getPAmt(); x++) { // for all people
					Person p = myBuilding.getPerson(x);
					if (((event.getX() > (p.getPos().getX() * 5) - 10) && event.getX() < (p.getPos().getX() * 5) + 10)
							&& ((event.getY() > (p.getPos().getY() * 5) - 10)
									&& event.getY() < (p.getPos().getY() * 5) + 10)) { // if the place clicked is in the
																						// range of a person
						gNum = x; // store the index of the person clicked
						if (graphShown == true) { // if the graphs are shown in the rtPane
							rmvGraphCanvas(); // remove them
						}
						addGraphCanvas(); // add the graphs
						if (hMetrics.isEmpty() == false) { // if there are metrics added to the person
							hGraph.yPlot("Hunger for Person " + (x + 1), hMetrics.get(x), 0); // plot all the graphs
							sGraph.yPlot("Sleep for Person " + (x + 1), sMetrics.get(x), 0);
							tGraph.yPlot("Thirst for Person " + (x + 1), tMetrics.get(x), 0);
							graphShown = true;
						}
						break;
					} else { // if the canvas is clicked not-on-a-person
						graphShown = false;
						gNum = -1;
						clearGraphs();
						rmvGraphCanvas(); // remove all the graphs
					}
				}
			}
		});
	};

	/**
	 * The main function of the GUI class, to launch the application
	 * 
	 * @param args the command line arguments passed to the arguments
	 */
	public static void main(String[] args) {
		Application.launch(args);
	}

}
